FROM node:14 as builder

ARG SHARED_USER
ARG SHARED_PASS
ARG APP_ENV
ARG GATEWAY_API_HOST
ARG CORE_API_HOST
ARG PUBLIC_MINIO_ENDPOINT
ARG PUBLIC_MINIO_KEY

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    fonts-liberation \
    libappindicator3-1 \
    libasound2 \
    libatk-bridge2.0-0 \
    libatk1.0-0 \
    libc6 \
    libcairo2 \
    libcups2 \
    libdbus-1-3 \
    libexpat1 \
    libfontconfig1 \
    libgbm1 \
    libgcc1 \
    libglib2.0-0 \
    libgtk-3-0 \
    libnspr4 \
    libnss3 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libstdc++6 \
    libx11-6 \
    libx11-xcb1 \
    libxcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxss1 \
    libxtst6 \
    lsb-release \
    wget \
    gcc \
    autoconf \
    libglu1 \
    libtool && rm -rf /var/lib/apt/lists/*

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list && apt update && \
    apt-get install google-chrome-stable -y && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

WORKDIR /build

COPY package.json package-lock.json .npmrc ./

RUN npm i

COPY . .

RUN npm run build

FROM nginx:alpine

COPY --from=builder /build/dist /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf

COPY default.conf /etc/nginx/conf.d/default.conf

RUN ln -s ../html /usr/share/nginx/html/richboy

ENTRYPOINT ["nginx", "-g", "daemon off;"]
