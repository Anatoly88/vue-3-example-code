module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/vue3-recommended", "@vue/typescript/recommended"],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    semi: "error",
    indent: ["error", 2],
    "arrow-parens": ["error", "as-needed"],
    "no-trailing-spaces": "error",
    "computed-property-spacing": ["error", "never"],
    "comma-spacing": ["error", { before: false, after: true }],
    "space-in-parens": ["error", "never"],
    "key-spacing": ["error"],
    "object-curly-spacing": ["error", "always"],
    "no-multi-spaces": ["error"],
    "space-unary-ops": 1,
    "space-infix-ops": ["error", { int32Hint: true }],
    "arrow-spacing": "error",
    "vue/component-tags-order": [
      "error",
      {
        order: ["route", "template", "script", "style"]
      }
    ],
    "vue/valid-define-props": "error",
    "vue/valid-define-emits": "error",
    "vue/static-class-names-order": "error",
    "vue/space-in-parens": ["error", "never"],
    "vue/object-curly-spacing": ["error", "always"],
    "vue/space-infix-ops": ["error", { int32Hint: true }],
    "vue/arrow-spacing": "error"
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)"
      ],
      env: {
        jest: true
      }
    }
  ]
};
