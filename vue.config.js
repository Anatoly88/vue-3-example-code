require('ts-node').register({
  project: './tsconfig.json',
  compilerOptions: {
    strict: false,
    module: "commonjs",
    lib: ["esnext"]
  }
})

module.exports = require("./vue.config.ts").config;
