const scanner = require("sonarqube-scanner");
scanner(
  {
    serverUrl: process.env.SQ_SERVER_URL,
    options: {
      "sonar.sources": "./src",
      "sonar.login": process.env.SQ_LOGIN,
      "sonar.password": process.env.SQ_PASSWORD,
    },
  },
  () => process.exit(),
);
