import { InventoryGrid } from "@gta5/gtanetsdk/dist/types/inventory";

export interface InventoryList {
  items: InventoryGrid[];
  limit: number;
}
