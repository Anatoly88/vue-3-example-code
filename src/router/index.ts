import { createMemoryHistory, createRouter, createWebHashHistory } from 'vue-router';
import routes from 'vue-auto-routing';
import { createRouterLayout } from 'vue-router-layout';

const RouterLayout = createRouterLayout(layout => {
  return import('@/layouts/' + layout + '.vue');
});

const routerHistory = process.env.LIB_MODE === 'true'
  ? createMemoryHistory()
  : createWebHashHistory(process.env.BASE_URL);

const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: '/',
      component: RouterLayout,
      children: routes,
    },
  ],
});

export default router;
