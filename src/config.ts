export const config = {
  gatewayApiHost: process.env.GATEWAY_API_HOST,
  coreApiHost: process.env.CORE_API_HOST,
  appEnv: process.env.APP_ENV
};
interface ScssConfig {
  scalingFactor: number;
  unit: string;
}

export const scssConfig: ScssConfig = {
  scalingFactor: 10.8,
  unit: "vh"
};

// convertor from px to viewport units
export type V = (px: number, sc?: ScssConfig) => string;
export const v: V = (px, sc = scssConfig) => px / sc.scalingFactor + sc.unit;

export type VNum = (px: number, sc?: ScssConfig) => number;
export const vnum: VNum = (px, sc = scssConfig) => px / sc.scalingFactor;
