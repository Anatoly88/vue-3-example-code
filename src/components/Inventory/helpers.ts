import { usePureCache } from '@/use/usePureCache';
import { CharacterGroupGridPermissionItem, InventoryItem } from '@gta5/gtanetsdk';

export const itemInteractProvideKey = 'itemInteractProvider';

export type ItemInteractFunction = (e: MouseEvent, item: InventoryItem) => void

export interface ItemInteractInjection {
  onMouseMove: ItemInteractFunction;
  onMouseEnter: ItemInteractFunction;
  onMouseLeave: ItemInteractFunction;
  onMouseDownRight: ItemInteractFunction;
  onMouseContextLeft: ItemInteractFunction;
}

export interface Position {
  x: number;
  y: number;
}

export interface GridFocus {
  id: string;
  cursor: Position;
  size: Position;
}

export interface CellType {
  index: number;
  occupied: boolean;
  x: number;
  y: number;
  item: InventoryItem | null;
  state: 'focused' | 'blocked' | null;
  isFocused: boolean;
}

export interface ItemSource {
  gridId: string;
  itemId: string;
}

export interface Drag {
  sourceContainer: HTMLElement | null;
  targetContainer: HTMLElement | null;
  cell: Position;
  size: Position;
  source: ItemSource;
  target: ItemSource;
}

export interface DropItemEvent {
  sourceGridId: string;
  targetGridId: string;
  sourceId: string;
  cell: Position;
}

export interface TargetDropItemEvent extends DropItemEvent {
  targetId: string;
}

export interface PlaceItem {
  id: string;
  type: string;
  name: string;
  img: string;
  distance: string;
}

export const getIdFromHtmlElement = (el: HTMLElement): string => el.id || el.dataset?.id || '';

const _getAvailableCells = (dimension: Position, cells: CellType[], columns: number): CellType[] => {
  const XyIndex = xyIndex({ x: dimension.x, y: dimension.y });
  let newCells: CellType[] = [];
  cells.find(baseCell => {
    if (baseCell.occupied) return false;
    if ((baseCell.x) + dimension.x > columns) return false;

    const targetCells = XyIndex
      .map(position => {
        const targetIdx = (baseCell.index + position.x) + (columns * position.y);
        return cells[targetIdx];
      })
      .filter(cell => !!cell && !cell.occupied);
    if (targetCells.length !== XyIndex.length) return false;
    newCells = targetCells;
    return true;
  });
  return newCells;
};

export const getAvailableCells = _getAvailableCells;

const _xyIndex = (dimension: Position): Position[] => {
  const cols = [...new Array(dimension.x)].map((el, idx) => idx);
  const rows = [...new Array(dimension.y)].map(() => cols);
  return rows
    .reduce((acc, arrX, y) => {
      return [...acc, ...arrX.map(x => ({ x, y }))];
    }, [] as Position[]);
};

export const xyIndex = usePureCache(_xyIndex);

export const getGridIndexByCoords = (cell: Position, columns: number): number => {
  return cell.y * columns + cell.x;
};

export const getGridItemIndexByCoords = (cell: Position, size: Position, columns: number): number[] => {
  if(size.x === 1 && size.y === 1) return [getGridIndexByCoords(cell, columns)];
  const coords = _xyIndex(size).map(_ => ({ x: _.x + cell.x, y: _.y + cell.y }));
  return coords.map(_ => getGridIndexByCoords(_, columns));
};

export const fillInventoryItemsGridId = (gridId: string, items: InventoryItem[]): InventoryItem[] => {
  return items.map((item: InventoryItem) => ({
    ...item,
    gridId,
  }));
};

export const defaultInventoryItemPermission = (): Partial<CharacterGroupGridPermissionItem> => ({
  pick: false,
  pickMaxItems: 1,
  pickMaxItemsResetPeriod: 3600,
  put: false,
  move: false,
});