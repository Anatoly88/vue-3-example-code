import { HistoryItem } from './../services/roulette';
import RouletteService from '@/services/roulette';
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";

const historyService = Container.get(RouletteService);

const VuexModule = createModule({
  namespaced: "roulette",
  strict: false
});

export default class RouletteModule extends VuexModule {
  @getter
  public historyList: HistoryItem[] | [] = [];

  @mutation
  setHistory(list: HistoryItem[]): void {
    this.historyList = list;
  }
  @action
  async getHistory(): Promise<void> {
    this.setHistory(await historyService.getHistory());
  }
}
