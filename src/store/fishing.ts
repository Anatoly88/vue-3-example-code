import { FishesList, FishesTypes } from "@/services/fishing";
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";
import FishingService from "@/services/fishing";

const fishingService = Container.get(FishingService);

const VuexModule = createModule({
  namespaced: "fishing",
  strict: false
});

export default class FishingModule extends VuexModule {
  @getter
  public fishes: FishesList | [] = [];
  public filters: FishesTypes[] | [] = [];
  @mutation
  setFishing(list: FishesList): void {
    this.fishes = list;
  }
  @mutation
  setFilters(list: FishesTypes[]): void {
    this.filters = list;
  }
  @action
  async getFishes(): Promise<void> {
    this.setFishing(await fishingService.getFishes());
  }
  @action
  async getFilters(): Promise<void> {
    this.setFilters(await fishingService.getFishFilter());
  }
}
