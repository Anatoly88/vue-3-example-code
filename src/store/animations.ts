import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";
import AnimationsService, { AnimationList } from "@/services/animations";

const animationService = Container.get(AnimationsService);

const VuexModule = createModule({
  namespaced: "animations",
  strict: false
});

export default class AnimationModule extends VuexModule {
  @getter
  public animations: AnimationList[] | [] = [];
  @mutation
  setAnimations(list: AnimationList[]): void {
    this.animations = list;
  }
  @action
  async getAnimations(): Promise<void> {
    this.setAnimations(await animationService.getAnimations());
  }
}
