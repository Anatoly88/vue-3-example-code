import { BetGamerItem } from './../services/bets';
import BetsService from '@/services/bets';
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";

const betsService = Container.get(BetsService);

const VuexModule = createModule({
  namespaced: "bets",
  strict: false
});

export default class BetsModule extends VuexModule {
  @getter
  public betsList: BetGamerItem[] | [] = [];
  @mutation
  setBets(list: BetGamerItem[]): void {
    this.betsList = list;
  }
  @action
  async getBets(): Promise<void> {
    this.setBets(await betsService.getBetsGamersList());
  }
}
