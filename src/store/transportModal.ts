import { Container } from '@gta5/vueplugin';
import { action, createModule, getter, mutation } from 'vuex-class-component';
import TransportModalService, { GradeType, TransportModalInfo } from '@/services/transportModal';

const transportModalService = Container.get(TransportModalService);

const VuexModule = createModule({
  namespaced: 'transportModal',
  strict: false,
});

export default class TransportModalModule extends VuexModule {
  @getter
  public transportInfo: TransportModalInfo | null = null;
  @mutation
  setTransportInfo(info: TransportModalInfo): void {
    this.transportInfo = info;
  }
  @action
  async getTransportInfo(): Promise<void> {
    this.setTransportInfo(await transportModalService.getTransportInfo());
  }
  @action
  async setGrades(grades: Array<GradeType>): Promise<void> {
    await transportModalService.setGrades(grades);
  }
}
