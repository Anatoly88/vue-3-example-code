import ItemsService, { IItem } from '@/services/items';
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";

const itemsService = Container.get(ItemsService);

const VuexModule = createModule({
  namespaced: "items",
  strict: false
});

export default class ItemsModule extends VuexModule {
  @getter
  public items: IItem[] | [] = [];
  @mutation
  setItems(list: IItem[]): void {
    this.items = list;
  }
  @action
  async getItemsList(): Promise<void> {
    this.setItems(await itemsService.getItemsList());
  }
}
