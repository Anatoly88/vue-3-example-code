import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";
import RadioService, { StationsList } from "@/services/radio";

const radioService = Container.get(RadioService);

const VuexModule = createModule({
  namespaced: "radio",
  strict: false
});

export default class RadioModule extends VuexModule {
  @getter
  public stations: StationsList | null = null;
  @mutation
  setRadioStations(list: StationsList): void {
    this.stations = list;
  }
  @action
  async getRadioStations(): Promise<void> {
    this.setRadioStations(await radioService.getStations());
  }
}
