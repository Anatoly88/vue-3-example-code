import { IGamer, ITableCard } from './../services/poker';
import PokerService from '@/services/poker';
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";

const pokerService = Container.get(PokerService);

const VuexModule = createModule({
  namespaced: "poker",
  strict: false
});

export default class PokerModule extends VuexModule {
  @getter
  public table: ITableCard[] | [] = [];

  @getter
  public gamerFirst: IGamer | null = null;
  @getter
  public gamerSecond: IGamer | null = null;
  @getter
  public gamerThree: IGamer | null = null;
  @getter
  public gamerFour: IGamer | null = null;
  @getter
  public user: IGamer | null = null;

  @mutation
  setTable(list: ITableCard[]): void {
    this.table = list;
  }
  @mutation
  setGamerFirst(gamer: IGamer): void {
    this.gamerFirst = gamer;
  }
  @mutation
  setGamerSecond(gamer: IGamer): void {
    this.gamerSecond = gamer;
  }
  @mutation
  setGamerThree(gamer: IGamer): void {
    this.gamerThree = gamer;
  }
  @mutation
  setGamerFour(gamer: IGamer): void {
    this.gamerFour = gamer;
  }
  @mutation
  setGamerUser(gamer: IGamer): void {
    this.user = gamer;
  }

  @action
  async getTableCards(): Promise<void> {
    this.setTable(await pokerService.getTable());
  }

  @action
  async getGamerUser(): Promise<void> {
    this.setGamerUser(await pokerService.getUser());
  }

  @action
  async getGamer(id: string): Promise<void> {
    switch (id) {
    case '1':
      this.setGamerFirst(await pokerService.getGamer(id));
      break;
    case '2':
      this.setGamerSecond(await pokerService.getGamer(id));
      break;
    case '3':
      this.setGamerThree(await pokerService.getGamer(id));
      break;
    case '4':
      this.setGamerFour(await pokerService.getGamer(id));
      break;
    default:
      break;
    }
  }
}
