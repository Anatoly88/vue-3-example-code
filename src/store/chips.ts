import { FreeCard } from './../services/chips';
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";
import ChipsService, { IChipsCard } from '@/services/chips';

const chipsService = Container.get(ChipsService);

const VuexModule = createModule({
  namespaced: "chips",
  strict: false
});

export default class ChipsModule extends VuexModule {
  @getter
  public chipsCardList: IChipsCard[] | [] = [];
  public chipsFreeList: FreeCard[] | [] = [];

  @mutation
  setChips(list: IChipsCard[]): void {
    this.chipsCardList = list;
  }
  @mutation
  setFreeChips(list: FreeCard[]): void {
    this.chipsFreeList = list;
  }
  @action
  async getChipsList(): Promise<void> {
    this.setChips(await chipsService.getChipsList());
  }
  @action
  async getFreeList(): Promise<void> {
    this.setFreeChips(await chipsService.getFreeList());
  }
}
