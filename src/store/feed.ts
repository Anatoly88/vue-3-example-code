import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";
import FeedService, { FeedItem } from '@/services/feed';

const feedService = Container.get(FeedService);

const VuexModule = createModule({
  namespaced: "feed",
  strict: false
});

export default class FeedModule extends VuexModule {
  @getter
  public feedList: FeedItem[] | [] = [];
  @mutation
  setFeed(list: FeedItem[]): void {
    this.feedList = list;
  }
  @action
  async getFeedList(): Promise<void> {
    this.setFeed(await feedService.getFeedList());
  }
}
