import { createStore } from "vuex";
import { createProxy, extractVuexModule } from "vuex-class-component";
import InventoryModule from "@/store/inventory";
import PrisonModalModule from "@/store/prisonModal";
import TransportModalModule from "./transportModal";
import FishingModule from "@/store/fishing";
import RadioModule from "@/store/radio";
import AnimationModule from "@/store/animations";
import AdsModule from "@/store/ads";
import BuffsModule from './buffs';
import CargoModule from './cargo';
import FeedModule from './feed';
import ChipsModule from './chips';
import RouletteModule from './roulette';
import BetsModule from './bets';
import ItemsModule from './items';
import SlotsModule from './slots';
import PokerModule from './poker';
import AreasModule from './areas';

const store = createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    ...extractVuexModule(InventoryModule),
    ...extractVuexModule(TransportModalModule),
    ...extractVuexModule(PrisonModalModule),
    ...extractVuexModule(FishingModule),
    ...extractVuexModule(RadioModule),
    ...extractVuexModule(AnimationModule),
    ...extractVuexModule(AnimationModule),
    ...extractVuexModule(AdsModule),
    ...extractVuexModule(BuffsModule),
    ...extractVuexModule(CargoModule),
    ...extractVuexModule(FeedModule),
    ...extractVuexModule(ChipsModule),
    ...extractVuexModule(RouletteModule),
    ...extractVuexModule(BetsModule),
    ...extractVuexModule(ItemsModule),
    ...extractVuexModule(SlotsModule),
    ...extractVuexModule(PokerModule),
    ...extractVuexModule(AreasModule),
  }
});

export default store;
export const storeProxy = {
  inventory: createProxy(store, InventoryModule),
  transportModal: createProxy(store, TransportModalModule),
  prisonModal: createProxy(store, PrisonModalModule),
  fishing: createProxy(store, FishingModule),
  radio: createProxy(store, RadioModule),
  animations: createProxy(store, AnimationModule),
  ads: createProxy(store, AdsModule),
  buffs: createProxy(store, BuffsModule),
  cargo: createProxy(store, CargoModule),
  feed: createProxy(store, FeedModule),
  chips: createProxy(store, ChipsModule),
  roulette: createProxy(store, RouletteModule),
  bets: createProxy(store, BetsModule),
  items: createProxy(store, ItemsModule),
  slots: createProxy(store, SlotsModule),
  poker: createProxy(store, PokerModule),
  areas: createProxy(store, AreasModule),
};
