import { PlaceItem } from './../components/Inventory/helpers';
import { action, createModule, getter, mutation } from "vuex-class-component";
import { Container } from "@gta5/vueplugin";
import InventoryMockService from "@/services/mock/inventory";
import InventoryService, {
  ExtendedMoveParams,
  ExtendedTargetMoveParams,
  InventoryItem
} from "@/services/inventory";
import { omit } from "lodash";
import {
  InventoryGrid,
  InventoryGridDistanceColor,
  InventoryGridSlotType
} from "@gta5/gtanetsdk";

const inventoryMockService = Container.get(InventoryMockService);
const inventoryService = Container.get(InventoryService);

const defaultGrid = (): InventoryGrid => ({
  id: "",
  type: InventoryGridSlotType.inventory,
  color: InventoryGridDistanceColor.red,
  title: "",
  description: "",
  distance: 0,
  limit: 100,
  items: [],
  restrictions: []
});

const VuexModule = createModule({
  namespaced: "inventory",
  strict: false
});

export default class InventoryModule extends VuexModule {
  @getter
  public grids: InventoryGrid[] = [];

  @getter
  public places: PlaceItem[] = [];

  get placesList(): PlaceItem[] {
    return this.places;
  }

  get inventoryGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "inventory") || defaultGrid();
  }

  get nearbyGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "nearby") || defaultGrid();
  }

  get hatGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "hat") || defaultGrid();
  }

  get eyesGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "eyes") || defaultGrid();
  }

  get earsGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "ears") || defaultGrid();
  }

  get neckGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "neck") || defaultGrid();
  }

  get outerwearGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "outerwear") || defaultGrid();
  }

  get tshirtGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "tshirt") || defaultGrid();
  }

  get maskGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "mask") || defaultGrid();
  }

  get wrist1Grid(): InventoryGrid {
    return this.grids.find(_ => _.type === "wrist1") || defaultGrid();
  }

  get wrist2Grid(): InventoryGrid {
    return this.grids.find(_ => _.type === "wrist2") || defaultGrid();
  }

  get pantsGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "pants") || defaultGrid();
  }

  get cartrunkGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "cartrunk") || defaultGrid();
  }

  get glovesGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "gloves") || defaultGrid();
  }

  get backpackGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "backpack") || defaultGrid();
  }

  get footwearGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "footwear") || defaultGrid();
  }

  get businessGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "business") || defaultGrid();
  }

  get garageGrid(): InventoryGrid {
    return this.grids.find(_ => _.type === "garage") || defaultGrid();
  }

  get quickAccessGrid(): InventoryGrid {
    // TODO type
    // @ts-ignore
    return this.grids.find(_ => _.type === "quick") || defaultGrid();
  }

  get exchangeUserGrid(): InventoryGrid {
    // TODO type
    // @ts-ignore
    return this.grids.find(_ => _.type === "exchangeUser") || defaultGrid();
  }

  get exchangeMyGrid(): InventoryGrid {
    // TODO type
    // @ts-ignore
    return this.grids.find(_ => _.type === "exchangeMy") || defaultGrid();
  }

  @mutation
  setGrids(grids: InventoryGrid[]): void {
    this.grids = grids;
  }

  @mutation
  setPlaces(places: PlaceItem[]): void {
    this.places = places;
  }

  @mutation
  addAllItems({
    gridId,
    items
  }: {
    gridId: string;
    items: InventoryItem[];
  }): void {
    const currentGrid: InventoryGrid =
      this.grids.find(_ => _.id === gridId) || defaultGrid();
    const inventoryGrid: InventoryGrid =
      this.grids.find(_ => _.type === "inventory") || defaultGrid();
    const lastIndex =
      inventoryGrid.items[inventoryGrid.items.length - 1].index[0];
    // TODO
    // при добавлении нужно что бы элементы вставали в конец грида с корректными параметрами расположения по x,y
    // первый элемент встают на позицию последний индекс инветаря + 1
    // все следующие элементы встают на позицию последний индекс предыдущего + X предидущего

    // items.reduce((pItem, cItem, index) => {
    //   pItem.index[0] = 3;
    //   cItem.index[0] = 6;
    //   return cItem;
    // });
    currentGrid.items = [];
    inventoryGrid.items.push(...items);
    if (!inventoryGrid) return;
  }

  @mutation
  addItem({ gridId, item }: { gridId: string; item: InventoryItem }): void {
    const grid = this.grids.find(_ => _.id === gridId);
    if (!grid) return;
    // console.group("mutation.addItem");
    // console.log({ gridId, item });
    // console.groupEnd();
    grid.items.push(item);
  }

  @mutation
  addOrReplaceItem({
    gridId,
    item
  }: {
    gridId: string;
    item: InventoryItem;
  }): void {
    const grid = this.grids.find(_ => _.id === gridId);
    if (!grid) return;
    const itemIndex = grid.items.findIndex(_ => _.id === item.id);
    if (itemIndex > 0) {
      grid.items.splice(itemIndex, 1);
    } else {
      grid.items.push(item);
    }
    console.group('mutation.addOrReplaceItem');
    console.log({ gridId, item, itemIndex });
    console.groupEnd();
  }

  @mutation
  removeItem({ gridId, itemId }: { gridId: string; itemId: number }): void {
    const gridIndex = this.grids.findIndex(_ => _.id === gridId);
    const grid = this.grids[gridIndex];
    if (gridIndex < 0 || !grid) return;
    const itemIndex = grid.items.findIndex(_ => _.id === itemId);
    if (itemIndex < 0) return;
    console.group("mutation.removeItem");
    console.log({ gridId, itemId, itemIndex });
    console.groupEnd();
    this.grids[gridIndex].items = [
      ...grid.items.slice(0, itemIndex),
      ...grid.items.slice(itemIndex + 1, grid.items.length)
    ];
  }

  @action
  async getItems(): Promise<void> {
    const grids = await inventoryMockService.getItems();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (grids.toString() == this.grids.toString()) return;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.setGrids(grids);
  }

  @action
  async getPlaces(): Promise<void> {
    const places = await inventoryMockService.getPlaces();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.setPlaces(places);
  }

  @action
  async moveItem(params: ExtendedMoveParams): Promise<void> {
    const item = this.grids
      .find(_ => _.id === params.sourceGridId)
      ?.items.find(_ => _.id === params.inventoryItemId);
    if (!item) return console.warn("cannot move item", { params });
    this.removeItem({
      gridId: params.sourceGridId,
      itemId: params.inventoryItemId
    });
    const loadingItem = {
      gridId: params.gridId,
      item: {
        ...item,
        loading: false,
        index: [params.index]
      }
    };
    this.addItem(loadingItem);
  }

  @action
  async stackItem(params: ExtendedTargetMoveParams): Promise<void> {
    try {
      const items = await inventoryService.moveItem(omit(params, 'sourceGridId', 'targetInventoryItemId'));
      this.removeItem({
        gridId: params.sourceGridId,
        itemId: params.inventoryItemId
      });
      this.removeItem({
        gridId: params.gridId,
        itemId: params.targetInventoryItemId
      });
      items.forEach((item: any) => this.addOrReplaceItem({
        gridId: item.gridId,
        item,
      }));
    } catch (err) {
      throw err;
    }
  }

  @action
  async moveAllNearbyItems(params: any): Promise<void> {
    try {
      // console.log('params action', params);
      this.addAllItems({ gridId: params.gridId, items: params.items });
    } catch (err) {
      throw err;
    }
  }
}
