import AreasService from '@/services/areas';
import { IMapData } from '@/services/areas';
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";

const areasService = Container.get(AreasService);

const VuexModule = createModule({
  namespaced: "areas",
  strict: false
});

export default class AreasModule extends VuexModule {
  @getter
  public mapData: IMapData | Record<string, never> = {};

  @mutation
  setMapData(map: IMapData): void {
    this.mapData = map;
  }

  @action
  async getMapData(): Promise<void> {
    this.setMapData(await areasService.getMapData());
  }
}
