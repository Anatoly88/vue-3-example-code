import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";
import CargoService, { CargoItem } from '@/services/cargo';

const cargoService = Container.get(CargoService);

const VuexModule = createModule({
  namespaced: "cargo",
  strict: false
});

export default class CargoModule extends VuexModule {
  @getter
  public cargo: CargoItem[] | [] = [];
  @mutation
  setCargo(list: CargoItem[]): void {
    this.cargo = list;
  }
  @action
  async getCargoList(): Promise<void> {
    this.setCargo(await cargoService.getCargoList());
  }
}
