import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";
import BuffsService, { BuffItem } from '@/services/buffs';

const buffsService = Container.get(BuffsService);

const VuexModule = createModule({
  namespaced: "buffs",
  strict: false
});

export default class BuffsModule extends VuexModule {
  @getter
  public buffs: BuffItem[] | [] = [];
  @mutation
  setBuffs(list: BuffItem[]): void {
    this.buffs = list;
  }
  @action
  async getBuffsList(): Promise<void> {
    this.setBuffs(await buffsService.getBuffsList());
  }
}
