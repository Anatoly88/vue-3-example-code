import { Container } from '@gta5/vueplugin';
import { action, createModule, getter, mutation } from 'vuex-class-component';
import PrisonModalService, { PrisonModalInfo } from '@/services/prisonModal';

const prisonModalService = Container.get(PrisonModalService);

const VuexModule = createModule({
  namespaced: 'prisonModal',
  strict: false,
});

export default class PrisonModalModule extends VuexModule {
  @getter
  public prisonInfo: PrisonModalInfo | null = null;
  @mutation
  setPrisonInfo(info: PrisonModalInfo): void {
    this.prisonInfo = info;
  }
  @action
  async getPrisonInfo(): Promise<void> {
    this.setPrisonInfo(await prisonModalService.getPrisonInfo());
  }
}
