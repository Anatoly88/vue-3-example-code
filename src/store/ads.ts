import { AdsItem } from './../services/ads';
import AdsService from '@/services/ads';
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";

const adsService = Container.get(AdsService);

const VuexModule = createModule({
  namespaced: "ads",
  strict: false
});

export default class AdsModule extends VuexModule {
  @getter
  public ads: AdsItem[] | [] = [];
  @mutation
  setAds(list: AdsItem[]): void {
    this.ads = list;
  }
  @action
  async getAdsList(): Promise<void> {
    this.setAds(await adsService.getAdsList());
  }
}
