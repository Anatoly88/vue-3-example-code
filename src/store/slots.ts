import { ICombinationItem } from './../services/slots';
import SlotsService from '@/services/slots';
import { Container } from "@gta5/vueplugin";
import { action, createModule, getter, mutation } from "vuex-class-component";

const slotsService = Container.get(SlotsService);

const VuexModule = createModule({
  namespaced: "slots",
  strict: false
});

export default class SlotsModule extends VuexModule {
  @getter
  public combinationsX2: ICombinationItem[] | [] = [];

  @getter
  public combinationsX3: ICombinationItem[] | [] = [];

  @mutation
  setCombinationsX2(list: ICombinationItem[]): void {
    this.combinationsX2 = list;
  }

  @mutation
  setCombinationsX3(list: ICombinationItem[]): void {
    this.combinationsX3 = list;
  }

  @action
  async getCombinationsX2(): Promise<void> {
    this.setCombinationsX2(await slotsService.getCombinationsX2());
  }

  @action
  async getCombinationsX3(): Promise<void> {
    this.setCombinationsX3(await slotsService.getCombinationsX3());
  }
}
