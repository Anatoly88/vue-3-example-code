import { useTranslate } from '@/use/useTranslate';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';

dayjs.extend(duration);

export function paginateArray<T>(array: Array<T>, pageSize: number, page:number): Array<T> {
  return array.slice((page - 1) * pageSize, page * pageSize);
}

export const formatNumber: (v: number) => string = (val?: number) =>
  val
    ?.toFixed(0)
    ?.toString()
    ?.replace(/\B(?=(\d{3})+(?!\d))/g, ' ') || '';

export function getFormattedDate(date: Date): string {
  const year = date.getFullYear();

  return getShortFormattedDate(date) + '.' + year;
}

export function getShortFormattedDate(date: Date): string {
  const month = (1 + date.getMonth()).toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');

  return day + '.' + month;
}

export function getFormattedTime(date: Date): string {
  const hours = date.getUTCHours().toString().padStart(2, '0');
  const minutes = date.getUTCMinutes().toString().padStart(2, '0');

  return hours + ':' + minutes;
}

export function getFormattedDateTime(date: Date, short = false): string {
  return `${short ? getShortFormattedDate(date) : getFormattedDate(date)}${short ? ',' : ''} ${getFormattedTime(date)}`;
}


export function getHMSFromSeconds(seconds: number): [number, number, number] {
  const hours = Math.floor(seconds / 3600);
  const secondsLeft = seconds % 3600;
  const minutes = Math.floor(secondsLeft / 60);
  return [hours, minutes, secondsLeft % 60];
}

export function wait(ms: number): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function simpleWrapWait<RETURNS = unknown>(func: () => Promise<RETURNS>, waitTime = 200): () => Promise<RETURNS> {
  let isWaiting = false;
  let returnPromise: Promise<RETURNS> | null = null;

  return async function(): Promise<RETURNS> {
    const isFirst = returnPromise === null;

    if(!isFirst && isWaiting) return returnPromise as Promise<RETURNS>;

    const rPromise: Promise<RETURNS> = new Promise(async (resolve, reject) => {
      isWaiting = true;
      await wait(waitTime);
      isWaiting = false;
      try {
        const data = await func.call(func);
        resolve(data);
      } catch(err) {
        reject(err);
      } finally {
        returnPromise = null;
      }
    });

    returnPromise = rPromise;
    return rPromise;

  };
}

export function wrapWait<IDS extends number | string, RETURNS = unknown>(func: (items: IDS[]) => Promise<RETURNS>, waitTime = 200): (items: IDS[]) => Promise<RETURNS> {
  const currentIds: IDS[] = [];
  let isWaiting = false;
  let returnPromise: Promise<RETURNS> | null = null;

  return async function(ids: IDS[]): Promise<RETURNS> {
    const isFirst = returnPromise === null;

    ids.filter(_ => !currentIds.includes(_)).forEach(id => currentIds.push(id));
    if(!isFirst && isWaiting) return returnPromise as Promise<RETURNS>;

    const rPromise: Promise<RETURNS> = new Promise(async (resolve, reject) => {
      isWaiting = true;
      await wait(waitTime);
      isWaiting = false;
      try {
        const data = await func.call(func, currentIds);
        resolve(data);
      } catch(err) {
        reject(err);
      } finally {
        returnPromise = null;
        currentIds.splice(0, currentIds.length);
      }
    });

    returnPromise = rPromise;
    return rPromise;

  };
}



export function powerfulDebounce<ArgumentsType extends unknown[], ReturnType, FnThis = unknown>(
  fn: (...args: ArgumentsType) => PromiseLike<ReturnType>,
  debounceWait: number,
): (...args: ArgumentsType) => Promise<ReturnType> {
  let isWaiting = false;
  let returnPromise: PromiseLike<ReturnType> | null = null;
  let lastCalled = 0;

  return async function(this: FnThis, ...args: ArgumentsType): Promise<ReturnType> {

    const now = new Date().getTime();
    const timeFromPreviousCall = now - lastCalled;
    if(timeFromPreviousCall < debounceWait && returnPromise) return returnPromise;

    const isFirst = returnPromise === null;
    if(!isFirst && isWaiting) return returnPromise as Promise<ReturnType>;

    lastCalled = now;
    const rPromise: Promise<ReturnType> = new Promise(async (resolve, reject) => {
      isWaiting = true;
      try {
        const data = await fn.call(this, ...args);
        resolve(data);
      } catch(err) {
        reject(err);
      } finally {
        isWaiting = false;
        returnPromise = null;
      }
    });

    returnPromise = rPromise;
    return rPromise;

  };
}

export function cropBalance(value: number): string {
  if (!value) {
    return '0';
  }

  const { translate } = useTranslate();

  const preparedValue = value.toString();
  const symbolsInValue = preparedValue.length;

  switch (true) {
  case symbolsInValue >= 16:
    return translate('personal.value_quadrillion', [{
      id: 0,
      type: 'number',
      value: preparedValue.slice(0, symbolsInValue - 15),
    }]).toUpperCase();
  case symbolsInValue >= 13:
    return translate('personal.value_trillion', [{
      id: 0,
      type: 'number',
      value: preparedValue.slice(0, symbolsInValue - 12),
    }]).toUpperCase();
  case symbolsInValue >= 10:
    return translate('personal.value_kkk', [{
      id: 0,
      type: 'number',
      value: preparedValue.slice(0, symbolsInValue - 9),
    }]).toUpperCase();
  case symbolsInValue >= 7:
    return translate('personal.value_kk', [{
      id: 0,
      type: 'number',
      value: preparedValue.slice(0, symbolsInValue - 6),
    }]).toUpperCase();
  case symbolsInValue >= 4:
    return translate('personal.value_k', [{
      id: 0,
      type: 'number',
      value: preparedValue.slice(0, symbolsInValue - 3),
    }]).toUpperCase();
  default:
    return preparedValue;
  }
}

export const getSecondsDuration: (seconds: number) => string = (seconds: number) => {
  const val = dayjs.duration(seconds * 1000);
  const hours = val.hours();
  const minutes = val.minutes();
  const days = val.days();
  const months = val.months();
  return `${months ? months + ' м. ' : ''}${days ? days + ' д. ' : ''}${
    hours ? hours + ' ч. ' : ''
  }${minutes} мин.`;
};

export const skipNotNumber = (e: KeyboardEvent, limit = 9999999999999): void => {
  if (!/\d|Backspace|Delete|ArrowRight|ArrowLeft/.test(e.key)) e.preventDefault();
  const target = e.target as HTMLInputElement;
  const currentValue = target.value;
  const key = e.key;
  const newValue = currentValue + key;
  if((parseInt(newValue) || 0) > limit) e.preventDefault();
};

export function vh(v: number): number {
  const h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  return (v * h) / 100;
}

export function getNoun(number: number, one: string, two: string, five: string): string {
  let n = Math.abs(number);
  n %= 100;
  if (n >= 5 && n <= 20) {
    return five;
  }
  n %= 10;
  if (n === 1) {
    return one;
  }
  if (n >= 2 && n <= 4) {
    return two;
  }
  return five;
}
export const getCallHelpCountNoun = (count: number): string => getNoun(count, 'патруль', 'патруля', 'патрулей');
export const getMetersNoun = (count: number): string => getNoun(count, 'метр', 'метра', 'метров');

