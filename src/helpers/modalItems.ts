import { formatNumber } from '@/helpers';

export interface Icon {
  name: string,
  type: 'png' | 'svg',
  style?: {
    size?: number,
    fill?: string,
    stroke?: string,
    strokeWidth?: number
  },
}

export interface InfoItem {
  value: string,
  title: string,
  prefix?: Icon,
  suffix?: Icon,
}

// Генератор элементов модального окна (driverPassport, transport)
export function buildModalItem(title: string, value: string, prefix?: Icon, suffix?: Icon): InfoItem {
  return {
    title: title,
    value: Number.isInteger(+value) ? formatNumber(parseInt(value)) : value,
    prefix: prefix,
    suffix: suffix,
  };
}
