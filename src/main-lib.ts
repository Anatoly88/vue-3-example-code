import { createWebType, DefaultCreateWebParams, DefaultCreateWebReturning, Gta5NetPlugin } from '@gta5/vueplugin';
import { GtaNetSDK } from '@gta5/gtanetsdk';

import { config } from '@/config';
import naive from 'naive-ui';

import { createApp } from 'vue';

export const createWeb: createWebType<DefaultCreateWebParams, DefaultCreateWebReturning>
  = async function (appSelector = '#app', sdk?: GtaNetSDK) {
    await Gta5NetPlugin.initSDK(config, sdk);

    const store = (await import('./store')).default;
    const router = (await import('./router')).default;
    const { default: App } = await import('./App.vue');
    const app = createApp(App);

    app
      .use(store)
      .use(router)
      .use(naive)
      .use(Gta5NetPlugin, {
        router,
        translateGroupKeys: ['baseapp', 'errors'],
      })
      .mount(appSelector);

    return {
      routePush: (path: string) => router.push(path),
    };
  };


