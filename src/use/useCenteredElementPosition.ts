import { computed, onMounted, Ref, ref } from 'vue';

export type MarginFunction = (value: number) => number

export function useCenteredElementPosition(element: Ref<HTMLElement | undefined>, margin?: { x?: MarginFunction, y?: MarginFunction }): { centerPosition: Ref<{ x: number, y: number }> } {

  const centerPositionRef = ref< { x: number, y: number }>({ x: 0, y: 0 });
  const centerPosition = computed(() => {
    const val = { ...centerPositionRef.value };
    if(margin?.x)
      val.x = margin.x(val.x);
    if(margin?.y)
      val.y = margin.y(val.y);

    return val;
  });

  const getCenteredPosition = () => {
    const el = element.value;
    if(!el) {
      console.warn('cannot find element to get center position');
      return {
        x: 0,
        y: 0,
      };
    }
    const rect = el.getBoundingClientRect();
    const centerX = rect.width / 2;
    const centerY = rect.height / 2;
    const pos = {
      x: centerX + rect.left,
      y: centerY + rect.top,
    };
    return pos;
  };
  onMounted(() => {
    centerPositionRef.value = getCenteredPosition();
  });

  return {
    centerPosition,
  };
}
