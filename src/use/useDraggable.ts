import { Draggable, DraggableContainer, DroppableOptions } from '@shopify/draggable';
import { onUnmounted } from 'vue';

export interface UseDraggableReturns {
  draggable: Draggable
}

export function useDraggable(containers: DraggableContainer, options?: DroppableOptions): UseDraggableReturns {
  const draggable = new Draggable(containers, options);

  onUnmounted(() => {
    draggable.destroy();
  });

  return {
    draggable,
  };
}
