type ClipboardFunction = (text: string) => void

function useClipboard(): { toClipboard: ClipboardFunction } {
  return {
    toClipboard(text: string) {
      window.postMessage({
        eventName: 'Helpers:CopyToClipboard',
        text,
      }, '*');
      // console.info('copying', { text });
      // const body = document.documentElement.querySelector('body');
      // if (!body) return;
      // const inputCopy = document.createElement(
      //   'INPUT',
      // ) as HTMLInputElement;
      // inputCopy.value = text;
      // body.appendChild(inputCopy);
      // inputCopy.select();
      // document.execCommand('copy');
      // body.removeChild(inputCopy);
    },
  };
}

export default useClipboard;
