import { computed } from 'vue';

import { RouteRecordRaw, useRouter, Router } from 'vue-router';

import { storeProxy } from '@/store';

type EmitFn<A extends string = string> = (action: A) => void
type CloseEmitFn = EmitFn<'close'>

const useLayout = (emit: CloseEmitFn, path: string) => {
  const router: Router = useRouter();

  const routes = computed(() => {
    return [...router.options.routes].shift()?.children
      ?.filter((r: RouteRecordRaw) => {
        return !!r?.meta?.tab && path == r?.meta?.layout;
      }).sort((a: RouteRecordRaw, b: RouteRecordRaw) => parseInt(a.meta?.tab as string) - parseInt(b.meta?.tab as string));
  });

  // const balance = computed(() => storeProxy.character.balance ?? 0);
  // const organizationBalance = computed(() => storeProxy.organization.organizationBalance ?? 0);

  // const avatar = computed(() =>
  //   path === 'organization'
  //     ? storeProxy.organization.organization?.avatar
  //     : storeProxy.character.character?.avatarUrl,
  // );

  const close = () => {
    emit('close');
  };

  return {
    // organizationBalance,
    // balance,
    // avatar,
    routes,
    close,
  };
};

export default useLayout;
