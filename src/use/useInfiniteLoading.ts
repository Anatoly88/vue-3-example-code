import { computed, nextTick, onBeforeMount, Ref, ref } from 'vue';
import { useHandleError } from '@gta5/vueplugin';
import { useRoute, useRouter } from 'vue-router';

export type UseInfiniteLoadingCallback = (offset: number, limit: number, initial: boolean) => Promise<void> | void

export interface UseInfiniteLoading {
  onScroll: (e: Event) => void;
  loading: Ref<boolean>;
  getInitialData: () => Promise<void>;
  page: Ref<number>;
  key: Ref<string>;
}

export type FunctionReturnsUnknownPromiseOrVoid = () => Promise<unknown> | void


//TODO:! придумать че-нить приятнее
export type ScrollBlockRef = Ref<{
  scrollTo: ({ left, top, behaviour }: { left: number, top: number, behaviour: ScrollBehavior }) => void,
  $el: {
    nextSibling: {
      firstChild: HTMLElement
    }
  }
}>

export interface UseInfiniteLoadingOptions {
  limit: number;
  total: () => number;
  currentTotal: () => number;
  scrollRef?: ScrollBlockRef,
  initial?: boolean;
  onBeforeInitial?: FunctionReturnsUnknownPromiseOrVoid;
  onWithInitial?: FunctionReturnsUnknownPromiseOrVoid;
  onAfterInitial?: FunctionReturnsUnknownPromiseOrVoid;
  initialKey?: string;
}

export function useInfiniteLoading(callback: UseInfiniteLoadingCallback, {
  limit = 6,
  total,
  currentTotal,
  scrollRef,
  initial,
  onBeforeInitial,
  onWithInitial,
  onAfterInitial,
  initialKey,
}: UseInfiniteLoadingOptions): UseInfiniteLoading {

  const router = useRouter();
  const route = useRoute();

  const { handleError } = useHandleError();

  const key = ref(initialKey || new Date().toString()); // ключ данных, позволяет юзать этот composable для получения разных данных из разных источников

  const loading = ref(false);
  const preventLoading = ref<Record<string, boolean>>({
    [key.value]: false,
  });
  const page = ref<Record<string, number>>({
    [key.value]: 1,
  });

  const pageManipulator = computed({
    get: () => page.value[key.value],
    set: (v: number) => page.value[key.value] = v,
  });

  const queryPage = computed(() => +(route.query.page || 1) || 1);

  const itemRows = () => {
    const t = currentTotal();
    return Math.ceil(t / 2); // т.к. 2 столбца
  };

  const setRouteQueryPage = (page: number) => {
    if(queryPage.value === page) return;
    router.push({
      query: {
        ...route.query,
        page,
      },
    });
  };

  const getPageByScrollTop = (scrollTop: number, scrollTotal: number) => {
    const page = (scrollTop * itemRows() / scrollTotal) / 3;
    return Math.max(Math.ceil(page), 1);
  };

  const getScrollTopByPage = (page: number, scrollTotal: number) => {
    return page / (page + 1) * scrollTotal;
  };

  const onScroll = (e: Event) => {
    const { srcElement } = e;
    if(!srcElement) return;
    const htmlElement = srcElement as unknown as HTMLElement;
    const isEnoughScrolled = (htmlElement.scrollTop + htmlElement.clientHeight) === htmlElement.scrollHeight;
    setRouteQueryPage(getPageByScrollTop(htmlElement.scrollTop + htmlElement.clientHeight, htmlElement.scrollHeight));
    if(isEnoughScrolled) getNextPage();
  };

  const getNextPage = async () => {
    const _key = key.value;
    if(preventLoading.value[_key] || loading.value || total() === currentTotal()) return;
    if(!page.value[_key]) page.value[_key] = 1;
    page.value[_key]++;
    loading.value = true;
    const lengthBefore = currentTotal();
    try {
      await callback((page.value[_key] - 1) * limit, limit, false);
      if(lengthBefore === currentTotal()) preventLoading.value[_key] = true;
    } catch (err) {
      console.error(err);
      handleError(err);
    } finally {
      loading.value = false;

    }
  };

  const getInitialData = async () => {
    const _key = key.value;
    loading.value = true;
    try {
      await callback(0, limit * queryPage.value, true);
      page.value[_key] = queryPage.value;
      if(scrollRef?.value && queryPage.value > 1)
        nextTick(() => {
          const scrollHeight = scrollRef.value.$el.nextSibling.firstChild.scrollHeight;
          const scroll = getScrollTopByPage(queryPage.value, scrollHeight);
          scrollRef.value.scrollTo({
            left: 0,
            top: scroll,
            behaviour: 'smooth',
          });
        });
    } catch (err) {
      console.error(err);
      handleError(err);
    } finally {
      loading.value = false;
    }
  };

  const initialize = async () => {
    loading.value = true;
    if(typeof onBeforeInitial === 'function')
      await onBeforeInitial();
    await Promise.all([
      getInitialData(),
      typeof onWithInitial === 'function' ? onWithInitial() : Promise.resolve(),
    ]);
    if(typeof onAfterInitial === 'function')
      await onAfterInitial();
  };


  onBeforeMount(async () => {
    if(initial)
      initialize();
  });


  return {
    onScroll,
    loading,
    getInitialData,
    key,
    page: pageManipulator,
  };
}
