// eslint-disable-next-line @typescript-eslint/no-var-requires
const hash = require('object-hash');

const defaultCacheKeyFunction = <ArgumentsType extends unknown[]>(...args: ArgumentsType): string => {
  return args.reduce((acc, item) => {
    if(typeof item === 'number' || typeof item === 'string' || Number.isInteger(item) || typeof item === 'boolean')
      return acc + (item as string).toString();
    const key = hash(item);
    return acc + key;
  }, '') as string;
};

export function usePureCache<ArgumentsType extends unknown[], ReturnType, FnThis = unknown>(
  fn: (...args: ArgumentsType) => ReturnType,
  cacheKeyFunction: (...args: ArgumentsType) => string = defaultCacheKeyFunction,
): (...args: ArgumentsType) => ReturnType {
  const cache: Record<string, ReturnType> = {};
  return function(this: FnThis, ...args: ArgumentsType): ReturnType {
    const cacheKey = cacheKeyFunction(...args);
    const cachedValue = cache[cacheKey];
    if(cachedValue) {
      // if(typeof cachedValue === 'object')
      //   return cloneDeep(cachedValue);
      // else
      return cachedValue;
    }
    const value = fn.call(fn, ...args);
    cache[cacheKey] = value;
    return value;
  };
}
