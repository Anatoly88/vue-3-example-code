import { TranslateService, Container } from '@gta5/vueplugin';

const translateService = Container.get(TranslateService);

export type TranslateFunction = (
  key: string,
  values: Parameters<typeof translateService.translate>[1]
) => string;

export function useTranslate(): { translate: TranslateFunction } {
  const translate: TranslateFunction = (
    key: string,
    values: Parameters<typeof translateService.translate>[1],
  ) => {
    return translateService.translate(key, values);
  };

  return {
    translate,
  };
}
