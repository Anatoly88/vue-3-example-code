type Location = 'ru-Ru';
type Type = 'number' | 'datetime';
type FormatNumber = (number: number) => string;
type FormatDate = (date: Date) => string;
type FixDateTimeFormatOptions = Intl.DateTimeFormatOptions & {dateStyle: 'short'};

function useFormat(type?: 'number', location?: Location): FormatNumber
function useFormat(type?: 'datetime', location?: Location): FormatDate
function useFormat(type: Type = 'number', location: Location = 'ru-Ru'): FormatNumber | FormatDate {
  if (type === 'number') {
    return new Intl.NumberFormat(location).format;
  }
  return (val: Date) => new Intl.DateTimeFormat(location, { dateStyle: 'short', timeStyle: 'short' } as FixDateTimeFormatOptions).format(val).replace(',', '');
}

export default useFormat;
