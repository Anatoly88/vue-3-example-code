import { Position } from '@/components/Inventory/helpers';
import { computed, ref, Ref, watch } from 'vue';

export function usePositionOffset(position: Ref<Position>, element: Ref<HTMLElement | null>, _offset = 15): { offset: Ref<{ top: string, left: string }> } {
  const offset = ref({
    left: '0px',
    top: '0px',
  });

  watch(() => position.value, () => {
    const left = isElementWidthInViewPort.value
      ? `${position.value.x + _offset}px`
      : `${position.value.x - (bounding.value?.width || 1) * computedWidthOffset.value - _offset}px`;
    const top = isElementHeightInViewPort.value
      ? `${position.value.y + _offset}px`
      : `${position.value.y - (bounding.value?.height || 1) * computedHeightOffset.value - _offset}px`;

    offset.value = { left, top };
  });


  const bounding = computed(() => element.value?.getBoundingClientRect());
  const isElementHeightInViewPort = computed(() => (bounding.value?.height || 0) + position.value.y + _offset < window.innerHeight);
  const isElementWidthInViewPort = computed(() => (bounding.value?.width || 0) + position.value.x + _offset < window.innerWidth);
  const computedHeightOffset = computed(() => {
    const offset = position.value.y - (bounding.value?.height || 0);
    return offset < 0 ? 0.5 : 1;
  });
  const computedWidthOffset = computed(() => {
    const offset = position.value.x - (bounding.value?.width || 0);
    return offset < 0 ? 0.5 : 1;
  });

  return {
    offset,
  };
}
