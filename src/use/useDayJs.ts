import dayjs from 'dayjs';
import dayjsRelativePlugin from 'dayjs/plugin/relativeTime';
import 'dayjs/locale/ru';
dayjs.extend(dayjsRelativePlugin);
dayjs.locale('ru');

export function useDayJs(time: number | Date | string): dayjs.Dayjs {
  return dayjs(time);
}
