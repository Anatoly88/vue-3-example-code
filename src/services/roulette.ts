import { Service } from '@gta5/vueplugin';

export interface HistoryItem {
  id: string;
  value: string;
  color: string;
}

@Service()
export default class RouletteService {
  async getHistory(): Promise<HistoryItem[]> {
    return [
      {
        id: '1',
        value: '12',
        color: 'red',
      },
      {
        id: '2',
        value: '4',
        color: 'black',
      },
      {
        id: '3',
        value: '12',
        color: 'red',
      },
      {
        id: '4',
        value: '4',
        color: 'black',
      },
      {
        id: '5',
        value: '12',
        color: 'red',
      },
      {
        id: '6',
        value: '4',
        color: 'black',
      },
      {
        id: '7',
        value: '12',
        color: 'red',
      },
      {
        id: '8',
        value: '4',
        color: 'black',
      },
    ];
  }
}
