import { Service } from '@gta5/vueplugin';

export interface PrisonAddFields {
  hours: string,
  minutes: string,
  reason: string
}

export interface PrisonModalInfo {
  name: string,
  avatar: string,
  article: string,
  timer: number,
  judgeName: string,
  judgeAvatar: string,
  deposit: string
}

@Service()
export default class PrisonModalService {
  async getPrisonInfo(): Promise<PrisonModalInfo> {
    return {
      name: 'Rey Mibourne',
      avatar: './mocks/img/avatar.png',
      article: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку',
      judgeAvatar: './mocks/img/avatar.png',
      judgeName: 'Lucas Simões',
      deposit: '45 219',
      timer: 360
    };
  }
}
