import { Service } from "@gta5/vueplugin";

export type FishFilterValue =
  | "trout"
  | "bass"
  | "yaz"
  | "pike"
  | "octopus"
  | "algae"
  | "carp";

export interface FishesCard {
  name: string;
  type: string;
  typeName: string;
  price: string;
  weight: string;
  img: string;
  filter: FishFilterValue;
  id: string;
}

export interface FishesList {
  id: string;
  items: FishesCard[];
}

export interface FishesTypes {
  label: string;
  value: FishFilterValue;
}

@Service()
export default class FishingService {
  async getFishes(): Promise<FishesList> {
    return {
      id: "1",
      items: [
        {
          id: "1",
          name: "Форель",
          type: "common",
          typeName: "распространенная",
          price: "45 219",
          weight: "22.1",
          img: "./mocks/img/fish.png",
          filter: "trout"
        },
        {
          id: "2",
          name: "Карась",
          type: "medium",
          typeName: "умеренная",
          price: "45 219",
          weight: "22.1",
          img: "./mocks/img/fish.png",
          filter: "carp"
        },
        {
          id: "3",
          name: "Окунь",
          type: "rare",
          typeName: "редкая",
          price: "45 219",
          weight: "22.1",
          img: "./mocks/img/fish.png",
          filter: "bass"
        },
        {
          id: "4",
          name: "Форель",
          type: "common",
          typeName: "распространенная",
          price: "45 219",
          weight: "22.1",
          img: "./mocks/img/fish.png",
          filter: "trout"
        },
        {
          id: "5",
          name: "Карась",
          type: "medium",
          typeName: "умеренная",
          price: "45 219",
          weight: "22.1",
          img: "./mocks/img/fish.png",
          filter: "carp"
        },
        {
          id: "6",
          name: "Окунь",
          type: "rare",
          typeName: "редкая",
          price: "45 219",
          weight: "22.1",
          img: "./mocks/img/fish.png",
          filter: "bass"
        }
      ]
    };
  }

  async getFishFilter(): Promise<FishesTypes[]> {
    return [
      {
        label: "Карась",
        value: "carp"
      },
      {
        label: "Окунь",
        value: "bass"
      },
      {
        label: "Язь",
        value: "yaz"
      },
      {
        label: "Щука",
        value: "pike"
      },
      {
        label: "Осьминог",
        value: "octopus"
      },
      {
        label: "Водоросли",
        value: "algae"
      }
    ];
  }
}
