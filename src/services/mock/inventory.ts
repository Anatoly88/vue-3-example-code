import { Service } from "@gta5/vueplugin";

@Service()
export default class MockedInventoryService {
  async getItems(): Promise<null> {
    return new Promise(async resolve => {
      const response = await fetch("/mockData/json/inventory.json");
      const data = await response.json();
      resolve(data);
    });
  }

  async getPlaces(): Promise<null> {
    return new Promise(async resolve => {
      const response = await fetch("/mockData/json/places.json");
      const data = await response.json();
      resolve(data);
    });
  }
}
