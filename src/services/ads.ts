import { Service } from '@gta5/vueplugin';

type AdsItemType = 'ads' | 'runline'

export interface AdsItem {
  id: string;
  name: string;
  avatarSrc: string;
  text: string;
  phone: string;
  type: AdsItemType;
  call: boolean;
}

@Service()
export default class AdsService {
  async getAdsList(): Promise<AdsItem[]> {
    return [
      {
        id: '1',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'ads',
        call: false
      },
      {
        id: '2',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'runline',
        call: false
      },
      {
        id: '3',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '',
        type: 'runline',
        call: false
      },
      {
        id: '4',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'runline',
        call: true
      },
      {
        id: '1',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'ads',
        call: false
      },
      {
        id: '2',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'runline',
        call: false
      },
      {
        id: '3',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '',
        type: 'runline',
        call: false
      },
      {
        id: '4',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'runline',
        call: true
      },
      {
        id: '1',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'ads',
        call: false
      },
      {
        id: '2',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'runline',
        call: false
      },
      {
        id: '3',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '',
        type: 'runline',
        call: false
      },
      {
        id: '4',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'runline',
        call: true
      },
      {
        id: '1',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'ads',
        call: false
      },
      {
        id: '2',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'runline',
        call: false
      },
      {
        id: '3',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '',
        type: 'runline',
        call: false
      },
      {
        id: '4',
        name: 'Lucas Simões',
        avatarSrc: './mocks/img/avatar.png',
        text: '105.1 УК РФ - Убийство, то есть умышленное причинение смерти другому человеку ',
        phone: '932-562-3139',
        type: 'runline',
        call: true
      },
    ];
  }
}
