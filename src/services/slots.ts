import { Service } from '@gta5/vueplugin';

export interface ICombinationItem {
  id: string;
  name: string;
  rate: string;
  count: number;
}

@Service()
export default class SlotsService {
  async getCombinationsX2(): Promise<ICombinationItem[]> {
    return [
      {
        id: '1',
        name: '2 сливы',
        rate: 'X2',
        count: 2
      },
      {
        id: '2',
        name: '2 гранаты',
        rate: 'X2',
        count: 2
      },
      {
        id: '3',
        name: '2 арбузы',
        rate: 'X3',
        count: 2
      },
      {
        id: '4',
        name: '2 носка',
        rate: 'X4',
        count: 2
      },
      {
        id: '5',
        name: '2 гранатомёта',
        rate: 'X5',
        count: 2
      },
      {
        id: '6',
        name: '2 колокола',
        rate: 'X6',
        count: 2
      },
    ];
  }

  async getCombinationsX3(): Promise<ICombinationItem[]> {
    return [
      {
        id: '1',
        name: '3 сливы',
        rate: 'X5',
        count: 3
      },
      {
        id: '2',
        name: '3 гранаты',
        rate: 'X5',
        count: 3
      },
      {
        id: '3',
        name: '3 арбуза',
        rate: 'X8',
        count: 3
      },
      {
        id: '4',
        name: '3 носка',
        rate: 'X10',
        count: 3
      },
      {
        id: '5',
        name: '3 носка',
        rate: 'X15',
        count: 3
      },
      {
        id: '6',
        name: '3 носка',
        rate: 'X30',
        count: 3
      },
    ];
  }
}
