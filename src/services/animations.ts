import { Service } from "@gta5/vueplugin";

export interface AnimationItem {
  id: string;
  name: string;
  src: string;
  shareLink: string;
}

export interface AnimationList {
  id: string;
  name: string;
  items: AnimationItem[];
}

@Service()
export default class AnimationsService {
  async getAnimations(): Promise<AnimationList[]> {
    return [
      {
        id: "1",
        name: "Общение",
        items: [
          {
            id: "1",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "2",
            name: "Анимация 3",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "3",
            name: "Анимация 4",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "4",
            name: "Анимация 5",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "5",
            name: "Анимация 6",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "6",
            name: "Анимация 7",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "7",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "8",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "9",
            name: "Анимация 7",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "10",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          },
          {
            id: "11",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          }
        ]
      },
      {
        id: "2",
        name: "Социальные",
        items: [
          {
            id: "1",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          }
        ]
      },
      {
        id: "3",
        name: "Сидеть",
        items: [
          {
            id: "1",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          }
        ]
      },
      {
        id: "4",
        name: "Лежать",
        items: [
          {
            id: "1",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          }
        ]
      },
      {
        id: "5",
        name: "Стоять",
        items: [
          {
            id: "1",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          }
        ]
      },
      {
        id: "6",
        name: "Танцы",
        items: [
          {
            id: "1",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          }
        ]
      },
      {
        id: "7",
        name: "Танцы",
        items: [
          {
            id: "1",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          }
        ]
      },
      {
        id: "8",
        name: "Танцы",
        items: [
          {
            id: "1",
            name: "Анимация 1",
            src: "./mocks/img/animation.png",
            shareLink: "https://google.com"
          }
        ]
      }
    ];
  }
}
