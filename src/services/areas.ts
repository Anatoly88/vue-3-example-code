import { Service } from '@gta5/vueplugin';

export interface IAreas {
  x: number
  y: number
  area: string
  color: string
  team: string
  teamLogo: string
  district: string
  status: string
  svg: string
}

export interface IMapData {
  image: string
  areas: IAreas[]
}

@Service()
export default class AreasService {
  async getMapData(): Promise<IMapData> {
    return {
      image: './mockData/images/war_map.jpg',
      areas: [
        {
          x: 162,
          y: 297,
          area: 'a1',
          color: '#1CC8FF',
          team: 'Empire',
          teamLogo: './mockData/images/lspd.png',
          district: 'Центральный район',
          status: 'Захватчик',
          svg: './mockData/images/war_area_1.svg'
        },
        {
          x: 278,
          y: 297,
          area: 'a2',
          color: '#FF9B04',
          team: 'Empire',
          teamLogo: './mockData/images/lspd.png',
          district: 'Центральный район',
          status: 'Захватчик',
          svg: './mockData/images/war_area_2.svg'
        },
        {
          x: 114,
          y: 60,
          area: 'a3',
          color: '#FF1C1C',
          team: 'Empire',
          teamLogo: './mockData/images/lspd.png',
          district: 'Центральный район',
          status: 'Захватчик',
          svg: './mockData/images/war_area_3.svg'
        }
      ]
    };
  }
}
