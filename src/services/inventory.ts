import {
  GtaNetSDK,
  InventoryItem as InventoryItemSrc,
  InventoryItemDetail,
  InventoryGrid,
  InventoryItemType,
  InventoryItemClass,
  LibraryItem
} from "@gta5/gtanetsdk";
import { SdkPlugin, Service } from "@gta5/vueplugin";
import { MoveItemParams } from "@gta5/gtanetsdk/types/inventory";
import { CarCharacterOwnership } from "@gta5/gtanetsdk/ex";
const $sdk: GtaNetSDK = SdkPlugin.sdk();

export interface InventoryItem extends Omit<InventoryItemSrc, "boxItems"> {
  boxItems: Array<{
    itemId: number;
    count: number;
    item?: InventoryItemDetail;
  }>;
  characterGroupItemType?: "kit" | "craft" | "car";
  idString?: string;
  loading?: boolean;
}

export interface ExtendedMoveParams extends MoveItemParams {
  sourceGridId: string;
}

export interface ExtendedTargetMoveParams extends ExtendedMoveParams {
  targetInventoryItemId: number;
}

export function characterCarToInventoryItem(
  car: CarCharacterOwnership,
  index?: number[]
): InventoryItem {
  return {
    actions: [],
    boxItems: [],
    activated: false,
    appearanceSlot: null,
    canActivate: false,
    canDrop: false,
    count: 0,
    gridId: "",
    id: 0,
    inCharacterExchange: false,
    index: index || [],
    item: {
      name: car.name,
      img: car.shopItem.images?.at(0) || car.shopItem.image,
      icon: car.shopItem.images?.at(0) || car.shopItem.image,
      x: 1,
      y: 1,
      type: InventoryItemType.accessory,
      class: InventoryItemClass.resource
    } as LibraryItem,
    quickSlot: null,
    returnableSubscriptionItem: false,
    characterGroupItemType: "car",
    idString: car.id
  };
}

@Service()
export default class InventoryService {
  async getItems(): Promise<InventoryGrid[]> {
    return (await $sdk.inventory.getItems()).grids;
  }

  async moveItem(params: MoveItemParams): Promise<InventoryItem[]> {
    return $sdk.inventory.moveItem(params);
  }

  async findItems(ids: number[]): Promise<LibraryItem[]> {
    return $sdk.inventory.getLibraryItemsByIds(ids);
  }

  async searchInventoryItems(query: string): Promise<LibraryItem[]> {
    return (
      await $sdk.inventory.getLibraryItems({
        pageSize: 25,
        skip: 0,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        query
      })
    ).items;
  }
}
