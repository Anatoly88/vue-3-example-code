import { Service } from '@gta5/vueplugin';

export interface IItem {
  id: string;
  name: string;
  img: string;
  price: string;
}

@Service()
export default class ItemsService {
  async getItemsList(): Promise<IItem[]> {
    return [
      {
        id: '1',
        name: 'Grey Shotguns',
        img: './mockData/images/shotgun.png',
        price: '6 890'
      },
      {
        id: '2',
        name: 'Grey Shotguns',
        img: './mockData/images/shotgun.png',
        price: '6 890'
      },
      {
        id: '3',
        name: 'Grey Shotguns',
        img: './mockData/images/shotgun.png',
        price: '6 890'
      },
      {
        id: '4',
        name: 'Grey Shotguns',
        img: './mockData/images/shotgun.png',
        price: '6 890'
      },
      {
        id: '5',
        name: 'Grey Shotguns',
        img: './mockData/images/shotgun.png',
        price: '6 890'
      },
      {
        id: '6',
        name: 'Grey Shotguns',
        img: './mockData/images/shotgun.png',
        price: '6 890'
      },
    ];
  }
}
