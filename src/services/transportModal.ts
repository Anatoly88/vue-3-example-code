import { Service } from '@gta5/vueplugin';

export enum GradeType {
  GRADE_1 = 'grade1',
  GRADE_2 = 'grade2',
  GRADE_3 = 'grade3',
  GRADE_4 = 'grade4',
  GRADE_5 = 'grade5',
  GRADE_6 = 'engine',
}

export interface TransportModalInfo {
  transportName: string,
  transportAvatar: string,
  transportNumber: string,
  ownerName: string,
  ownerAvatar: string,
  stats: {
    mileage: number,
    state: number,
    marketPrice: number,
    priceWithTuning: number,
  },
  grades: Array<GradeType>,
}

export const gradesTitles: Record<GradeType, {
  type: GradeType,
  title: string,
  icon: string,
}> = {
  [GradeType.GRADE_1]: {
    type: GradeType.GRADE_1,
    title: 'grade_1',
    icon: 'second',
  },
  [GradeType.GRADE_2]: {
    type: GradeType.GRADE_2,
    title: 'grade_2',
    icon: 'first',
  },
  [GradeType.GRADE_3]: {
    type: GradeType.GRADE_3,
    title: 'grade_3',
    icon: 'grade_3',
  },
  [GradeType.GRADE_4]: {
    type: GradeType.GRADE_4,
    title: 'grade_4',
    icon: 'grade_4',
  },
  [GradeType.GRADE_5]: {
    type: GradeType.GRADE_5,
    title: 'grade_5',
    icon: 'grade_5',
  },
  [GradeType.GRADE_6]: {
    type: GradeType.GRADE_6,
    title: 'Двигатель',
    icon: 'engine',
  },
};

@Service()
export default class TransportModalService {
  async getTransportInfo(): Promise<TransportModalInfo> {
    return {
      transportName: 'Truffade Nero Custom',
      transportAvatar: `./mocks/img/shop/cars/car${Math.round(Math.random() * 7 + 1)}.png`,
      transportNumber: 'XJ936jhs',
      ownerAvatar: './mocks/img/avatar.png',
      ownerName: 'Cyrus Raider',
      stats: {
        mileage: 43554,
        state: 65,
        marketPrice: 12500,
        priceWithTuning: 18000,
      },
      grades: [GradeType.GRADE_2],
    };
  }

  async setGrades(grades: Array<GradeType>): Promise<void> {
    console.log(`Устанавливаем грейды на тачку: ${grades}`);
  }
}
