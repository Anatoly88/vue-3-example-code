import { Service } from '@gta5/vueplugin';

export interface ITableCard {
  id: string;
  img: string;
}

export interface IGamer {
  id: string;
  avatar: string;
  name: string;
  cash: string;
  pass: boolean;
  thinks: boolean;
  winner: boolean;
  status: string;
  cards: ITableCard[];
}

@Service()
export default class PokerService {
  async getTable(): Promise<ITableCard[]> {
    return [
      {
        id: '1',
        img: './mockData/images/card_5.png'
      },
      {
        id: '2',
        img: './mockData/images/card_4.png'
      },
      {
        id: '3',
        img: './mockData/images/card_3.png'
      },
      {
        id: '4',
        img: './mockData/images/card_2.png'
      },
      {
        id: '5',
        img: './mockData/images/card_1.png'
      }
    ];
  }

  async getUser(): Promise<IGamer> {
    return {
      id: '10',
      avatar: './mockData/images/poker_user_5.jpg',
      name: 'Вы',
      cash: '100 546',
      pass: false,
      thinks: false,
      status: 'Добавил $ 45К',
      winner: false,
      cards: [
        {
          id: '1',
          img: './mockData/images/card_1.png'
        },
        {
          id: '2',
          img: './mockData/images/card_2.png'
        },
      ],
    };
  }

  async getGamer(id: string): Promise<IGamer | any> {
    switch (id) {
    case '1':
      return {
        id: '1',
        avatar: './mockData/images/poker_user_1.jpg',
        name: 'Enaya Chio',
        cash: '100 546',
        pass: false,
        thinks: false,
        status: '',
        winner: false,
        cards: [
          {
            id: '1',
            img: './mockData/images/card_1.png'
          },
          {
            id: '2',
            img: './mockData/images/card_2.png'
          },
        ],
      };
      break;
    case '2':
      return {
        id: '2',
        avatar: './mockData/images/poker_user_2.jpg',
        name: 'Enaya Chio',
        cash: '100 546',
        pass: true,
        thinks: false,
        status: 'Уравнял(а)',
        winner: false,
        cards: [
          {
            id: '1',
            img: './mockData/images/card_1.png'
          },
          {
            id: '2',
            img: './mockData/images/card_2.png'
          },
        ],
      };
      break;
    case '3':
      return {
        id: '3',
        avatar: './mockData/images/poker_user_3.jpg',
        name: 'Enaya Chio',
        cash: '100 546',
        pass: false,
        thinks: true,
        status: 'Думает',
        winner: false,
        cards: [
          {
            id: '1',
            img: './mockData/images/card_1.png'
          },
          {
            id: '2',
            img: './mockData/images/card_2.png'
          },
        ],
      };
      break;
    case '4':
      return {
        id: '4',
        avatar: './mockData/images/poker_user_4.jpg',
        name: 'Enaya Chio',
        cash: '100 546',
        pass: false,
        thinks: false,
        status: 'Победитель!',
        winner: true,
        cards: [
          {
            id: '1',
            img: './mockData/images/card_1.png'
          },
          {
            id: '2',
            img: './mockData/images/card_2.png'
          },
        ],
      };
      break;
    default:
      return null;
      break;
    }
  }
}
