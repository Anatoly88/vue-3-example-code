import { Service } from '@gta5/vueplugin';

type BuffType = 'buff' | 'debuff'

export interface BuffItem {
  id: string;
  name: string;
  description: string;
  img: string;
  type: BuffType
}

@Service()
export default class BuffsService {
  async getBuffsList(): Promise<BuffItem[]> {
    return [
      {
        id: '1',
        name: 'Drugs and chips',
        img: './mocks/img/buff.png',
        description: 'Описание баффа или дебаффа в 2 строки длиною',
        type: 'debuff'
      },
      {
        id: '2',
        name: 'Drugs and chips',
        img: './mocks/img/buff.png',
        description: 'Описание баффа или дебаффа в 2 строки длиною',
        type: 'buff'
      }
    ];
  }
}
