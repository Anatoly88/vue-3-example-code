import { Service } from "@gta5/vueplugin";

export interface RadioStation {
  id: string;
  name: string;
  linePrice: string;
  adsPrice: string;
}

export interface StationsList {
  id: string;
  items: RadioStation[];
}

@Service()
export default class RadioService {
  async getStations(): Promise<StationsList> {
    return {
      id: "1",
      items: [
        {
          id: "1",
          name: "Los Santos Radio",
          linePrice: "32",
          adsPrice: "150"
        },
        {
          id: "2",
          name: "Los Santos Radio",
          linePrice: "32",
          adsPrice: "150"
        },
        {
          id: "3",
          name: "Los Santos Radio",
          linePrice: "32",
          adsPrice: "150"
        },
        {
          id: "4",
          name: "Los Santos Radio",
          linePrice: "32",
          adsPrice: "150"
        },
        {
          id: "5",
          name: "Los Santos Radio",
          linePrice: "32",
          adsPrice: "150"
        },
        {
          id: "6",
          name: "Los Santos Radio",
          linePrice: "32",
          adsPrice: "150"
        },
        {
          id: "7",
          name: "Los Santos Radio",
          linePrice: "32",
          adsPrice: "150"
        },
        {
          id: "8",
          name: "Los Santos Radio",
          linePrice: "32",
          adsPrice: "150"
        }
      ]
    };
  }
}
