import { Service } from '@gta5/vueplugin';

export interface FeedItem {
  id: string;
  name: string;
  avatarSrc: string;
  equipSrc: string
  status: string;
}

@Service()
export default class FeedService {
  async getFeedList(): Promise<FeedItem[]> {
    return [
      {
        id: '1',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        equipSrc: './mockData/images/feed_1.png',
        status: 'Только что' // тут дату онлайна
      },
      {
        id: '2',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        equipSrc: './mockData/images/feed_2.png',
        status: 'Только что' // тут дату онлайна
      },
      {
        id: '3',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        equipSrc: './mockData/images/feed_3.png',
        status: 'Только что' // тут дату онлайна
      },
      {
        id: '4',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        equipSrc: './mockData/images/feed_4.png',
        status: 'Только что' // тут дату онлайна
      },
      {
        id: '5',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        equipSrc: './mockData/images/feed_1.png',
        status: 'Только что' // тут дату онлайна
      },
      {
        id: '6',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        equipSrc: './mockData/images/feed_2.png',
        status: 'Только что' // тут дату онлайна
      },
      {
        id: '7',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        equipSrc: './mockData/images/feed_3.png',
        status: 'Только что' // тут дату онлайна
      },
      {
        id: '8',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        equipSrc: './mockData/images/feed_4.png',
        status: 'Только что' // тут дату онлайна
      },
    ];
  }
}
