import { Service } from '@gta5/vueplugin';

export interface IChipsCard {
  id: string;
  title: string;
  subtitle: string;
  chips: string;
  fiat: string;
  imageColor: string;
}


export interface FreeCard {
  id: string;
  value: string;
  color: string
}

@Service()
export default class ChipsService {
  async getChipsList(): Promise<IChipsCard[]> {
    return [
      {
        id: '1',
        title: 'Новичок',
        subtitle: 'Стоит раз пропробовать',
        chips: '100',
        fiat: '10 000',
        imageColor: 'black'
      },
      {
        id: '2',
        title: 'Любитель',
        subtitle: 'Для ценителей игры',
        chips: '1 000',
        fiat: '100 000',
        imageColor: 'red'
      },
      {
        id: '3',
        title: 'Продвинутый',
        subtitle: 'Для тех кто любит риск',
        chips: '10 000',
        fiat: '1 000 000',
        imageColor: 'blue'
      },
      {
        id: '4',
        title: 'Новичок',
        subtitle: 'Для самых-самых',
        chips: '100 000',
        fiat: '10 000 000',
        imageColor: 'green'
      },
    ];
  }

  async getFreeList(): Promise<FreeCard[]> {
    return [
      {
        id: '1',
        value: '10 000',
        color: 'blue'
      }
    ];
  }
}
