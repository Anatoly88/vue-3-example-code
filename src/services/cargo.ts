import { Service } from '@gta5/vueplugin';


export interface CargoItem {
  id: string;
  address: string;
  cargo: string;
  countTotal: string;
  countCurrent: string;
}

@Service()
export default class CargoService {
  async getCargoList(): Promise<CargoItem[]> {
    return [
      {
        id: '1',
        address: 'Москва, ул. Ленина 12',
        cargo: 'Кирпичи',
        countCurrent: '21',
        countTotal: '32'
      },
      {
        id: '2',
        address: 'Москва, ул. Ленина 12',
        cargo: 'Кирпичи красивые',
        countCurrent: '21',
        countTotal: '32'
      },
    ];
  }
}
