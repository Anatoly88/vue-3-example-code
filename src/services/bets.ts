import { Service } from '@gta5/vueplugin';

export interface BetGamerItem {
  id: string;
  name: string;
  avatarSrc: string;
  cash: string;
  bet: number;
  betColor: string;
}

@Service()
export default class BetsService {
  async getBetsGamersList(): Promise<BetGamerItem[]> {
    return [
      {
        id: '1',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        cash: '10 000',
        bet: 33,
        betColor: 'red'
      },
      {
        id: '2',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        cash: '10 000',
        bet: 10,
        betColor: 'black'
      },
      {
        id: '3',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        cash: '10 000',
        bet: 7,
        betColor: 'red'
      },
      {
        id: '4',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        cash: '10 000',
        bet: 33,
        betColor: 'black'
      },
      {
        id: '5',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        cash: '10 000',
        bet: 33,
        betColor: 'red'
      },
      {
        id: '6',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        cash: '10 000',
        bet: 33,
        betColor: 'black'
      },
      {
        id: '7',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        cash: '10 000',
        bet: 33,
        betColor: 'red'
      },
      {
        id: '8',
        name: 'Rey Mibourne',
        avatarSrc: './mocks/img/avatar.png',
        cash: '10 000',
        bet: 33,
        betColor: 'black'
      },
    ];
  }
}
