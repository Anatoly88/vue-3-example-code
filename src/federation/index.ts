import { FederationApp, FederationAppContainer } from '@gta5/vueplugin';

export const adminAppContainer = new FederationAppContainer(
  // `https://${process.env.PUBLIC_MINIO_ENDPOINT}/assets`,
  'http://localhost:8080',
);

export const app: FederationApp = adminAppContainer.addFederationApp('baseapp');
app
  .addExposes('./src/main-lib');



