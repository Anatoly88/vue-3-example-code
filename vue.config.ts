import { scssConfig } from './src/config';
import { Compiler } from 'webpack';
import * as path from 'path';
import { app } from './src/federation';
import { ProjectOptions } from '@vue/cli-service';

const renderRoutes = ['/'];
const renderAfterElementExists = '.hello';
const webpack = require('webpack');
const { ModuleFederationPlugin } = require('webpack').container;
const packagejson = require('./package.json');

const pluginOptions: Record<string, any> = {};
const configureWebpack: Record<string, any> = {
  optimization: {
    splitChunks: false,
  },
  plugins: [],
};

let publicPath;
const ASSET_PATH = `${process.env.GATEWAY_API_HOST}/packages/files/`;
function PublicPathWebpackPlugin () {}

PublicPathWebpackPlugin.prototype.apply = function (compiler: any) {
  compiler.hooks.entryOption.tap('PublicPathWebpackPlugin', (context: any, entry: any) => {
    if (entry['module.common']) {
      entry['module.common'] = path.resolve(__dirname, './src/main.js');
    }
    if (entry['module.umd']) {
      entry['module.umd'] = path.resolve(__dirname, './src/main.js');
    }
    if (entry['module.umd.min']) {
      entry['module.umd.min'] = path.resolve(__dirname, './src/main.js');
    }
  });
  compiler.hooks.beforeRun.tap('PublicPathWebpackPlugin', (compiler: Compiler) => {
    compiler.options.output.publicPath = ASSET_PATH;
  });
};

// console.log({
//   appConfig: app.appConfig,
//   buildConfig: app.buildConfig,
// });

if(process.env.LIB_MODE !== 'true') {
  // pluginOptions.prerenderSpa =  {
  //   registry: undefined,
  //   customRendererConfig: {
  //     injectProperty: "appData",
  //     inject: {
  //       prerenderSpa: false,
  //     },
  //     // Optional - Wait to render until the specified element is detected using `document.querySelector`
  //     renderAfterElementExists,
  //   },
  //   renderRoutes,
  //   useRenderEvent: false,
  //   headless: true,
  //   onlyProduction: true,
  //   postProcess: (route) => {
  //     // Defer scripts and tell Vue it's been server rendered to trigger hydration
  //     route.html = route.html
  //       .replace(/<script (.*?)>/g, "<script $1 defer>")
  //       .replace(' class="Page">', ' class="Page" id="app" data-server-rendered="true">');
  //     return route;
  //   },
  // }
  configureWebpack.plugins.push(
    new ModuleFederationPlugin(app.appConfig),
  );
  pluginOptions.prerenderSpa = {};
} else {
  publicPath = ASSET_PATH;
  configureWebpack
    .plugins = [
      new ModuleFederationPlugin(app.buildConfig),
      // @ts-ignore
      new PublicPathWebpackPlugin(),
    ];
  configureWebpack
    .output = {
      library: {
        type: 'amd',
      },
    };
  // configureWebpack.externals = {
  //       ...config.get('externals'),
  //       vue: {
  //         commonjs: 'vue',
  //         commonjs2: 'vue',
  //         root: 'Vue',
  //         amd: 'vue'
  //       }
  //     }
  pluginOptions.prerenderSpa = {};

}

export const config: ProjectOptions = {
  lintOnSave: true,
  css: {
    extract: false,
    loaderOptions: {
      sass: {
        additionalData:
          `@function v($px) { @return #{($px / ${scssConfig.scalingFactor})}#{${scssConfig.unit}}; }` +
          '@import "@/assets/scss/_variables.scss";',
      },
    },
  },
  publicPath,
  chainWebpack: (config: any) => {
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();

    svgRule
      .test(/\.svg$/)
      .use('svg-url-loader')
      .loader('svg-url-loader');

    config.plugin('define').tap((definitions: any) => {
      definitions[0] = Object.assign(definitions[0], {
        'process.env.APP_ENV': JSON.stringify(process.env.APP_ENV),
        'process.env.GATEWAY_API_HOST': JSON.stringify(process.env.GATEWAY_API_HOST),
        'process.env.FAVICON_URL': JSON.stringify(process.env.FAVICON_URL),
      });
      return definitions;
    });
  },
  configureWebpack,
  pluginOptions,
};
