module.exports = {
  arrowParens: 'avoid',
  endOfLine: 'auto',
  trailingComma: 'none',
  singleQuotes: false,
};
